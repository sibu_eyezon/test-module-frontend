import { applyMiddleware, compose, createStore } from 'redux';

import thunk from 'redux-thunk';
import rootReducer from './reducers'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

const persistConfig = {
  key: 'magnox',
  storage,
}
const persistedReducer = persistReducer(persistConfig, rootReducer)
const middlewares = [applyMiddleware(thunk)];
if (window.__REDUX_DEVTOOLS_EXTENSION__) {
  middlewares.push(window.__REDUX_DEVTOOLS_EXTENSION__())
}

const store=createStore(
    persistedReducer,
    compose(...middlewares)    
)

export const  persistor = persistStore(store)
export default store;


