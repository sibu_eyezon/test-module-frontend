import React, { Component } from 'react';
import Form from './Form';
import { Redirect } from 'react-router-dom';
import classes from './Passcode.module.css';
import Logo from '../img/magnox-logo.png';
import { connect } from 'react-redux';
import { cancelAlert, login } from '../../actions/auth';
import { ToastContainer } from 'react-toastify';

class Passcode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      passcode: '',
      error: '',
    };
  }

  handleChange = (event) => {
    this.setState({ passcode: event.target.value });
  };
  handleSubmit = async (event) => {
    event.preventDefault();
    const error = this.validate();
    if (!error) {
      let passcode = this.state.passcode;
      this.props.login({ passcode });
    } else {
      this.setState({ error: error });
    }
  };

  handleKeyPress = (event) => {
    if (event.target.value) {
      this.setState({ error: '' });
    } else {
      this.setState({ error: 'Please enter your exam passcode' });
    }
  };

  validate = () => {
    let error = '';
    const passcode = this.state.passcode;
    if (!passcode) {
      error = 'Please enter your exam passcode';
    }
    return error;
  };

  render() {
    if (this.props.auth.isAuthenticated === true) {
      return <Redirect to="/details" />;
    }

    return (
      <div className="container">
        <div className={classes.Container_Box}>
          <div className="row">
            <div className="col-md-6 col-lg-5 col-sm-10 m-auto ">
              <ToastContainer />
              <div className="card p-3 bg-light">
                <div className="card-title text-center">
                  <img src={Logo} className="image-fluid" alt="" width="50%" />
                </div>
                <div className="card-body">
                  <Form
                    value={this.state.passcode}
                    error={this.state.error}
                    handleChange={this.handleChange}
                    handleSubmit={this.handleSubmit}
                    handleKeyPress={this.handleKeyPress}
                    isLoading={this.props.loading.isLoading}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const auth = state.auth;
  const alert = state.alert;
  const loading = state.loading;
  return {
    auth,
    alert,
    loading,
  };
};
const mapDispatchToProps = {
  login: (data) => login(data),
  cancelAlert: (data) => cancelAlert(data),
};
export default connect(mapStateToProps, mapDispatchToProps)(Passcode);
