import React, { Component } from 'react';
import Logo from '../img/magnox-logo.png';
import Dp from '../img/placeholder.jpg';
import { Redirect, Link } from 'react-router-dom';
import Section from './Section';
import { connect } from 'react-redux';
import { getTestDetails, getUserDetails } from '../../actions/details';
import { startTest } from '../../actions/test';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { cancelAlert } from '../../actions/auth';
class Details extends Component {
  _startExam = async () => {
    let id = this.props.auth.user.test_can;
    let duration = this.props.test.test.Time;
    let token = this.props.auth.user.token;

    this.props.startTest({ id, duration, token });
  };

  componentDidMount() {
    let { user_id, test_id, token } = this.props.auth.user;

    this.props.getUserDetails({ id: user_id, token });
    this.props.getTestDetails({ id: test_id, token });
  }

  render() {
    if (!this.props.auth?.user) {
      return <Redirect to="/" />;
    } else if (this.props.test.test_info) {
      return <Redirect to="/test" />;
    }

    return (
      <div className="container">
        <div className="row mt-4">
          <div className="col-md-5 m-auto">
            <ToastContainer />

            <div className="text-center">
              <img src={Logo} className="image-fluid" alt="" width="50%" />
            </div>
          </div>
        </div>
        <div className="row mt-4 ">
          <Section
            title="Student Details"
            img={this.props.auth.userDetails?.user_img || Dp}
            info={this.props.auth.userDetails?.user_info}
          />
          <Section
            title="Examination Details"
            date={`${new Date().toISOString().slice(0, 19).replace('T', ' ')}`}
            info={this.props.test?.test}
          />
        </div>
        <div className="row mt-2 mb-1 pt-2">
          <div className="col-md-5 offset-md-1 mt-1">
            <Link className="btn btn-danger btn-lg btn-block" to="/thankyou">
              Click to exit Examination
            </Link>
          </div>
          <div className="col-md-5 offset-md-1 mt-1">
            <button
              type="button"
              className="btn btn-primary btn-lg btn-block"
              onClick={this._startExam}
            >
              {this.props.loading.isLoading
                ? 'Loading...'
                : 'Click to start Examintion'}
            </button>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  const auth = state.auth;
  const alert = state.alert;
  const test = state.test;
  const loading = state.loading;
  return {
    auth,
    alert,
    test,
    loading,
  };
};
const mapDispatchToProps = {
  getUserDetails: (data) => getUserDetails(data),
  getTestDetails: (data) => getTestDetails(data),
  startTest: (data) => startTest(data),
  cancelAlert: (data) => cancelAlert(data),
};
export default connect(mapStateToProps, mapDispatchToProps)(Details);
