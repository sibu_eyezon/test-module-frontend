import moment from 'moment';
import React from 'react';

const Section = (props) => (
  <div className="col-md-5 offset-md-1 mt-2 ">
    <div className="card py-3 px-1 section-card">
      <h5 className="card-title text-center font-weight-bold ">
        {props.title}
      </h5>
      {props.img && (
        <img
          src={props.img}
          className="image-fluid rounded-circle mx-auto d-block text-center align-items-center"
          alt="Profile Picture"
          width="50%"
        />
      )}
      <div className="card-body ">
        <table className="table ">
          <tbody>
            {props.date && (
              <tr>
                <td>Date: </td>
                <td>
                  {moment
                    .utc(props.date)
                    .local()
                    .format('dddd, MMMM Do YYYY, h:mm:ss a')}
                </td>
              </tr>
            )}
            {props.info &&
              Object.keys(props.info).map((item, i) => (
                <tr key={i}>
                  <td>{item}: </td>
                  <td className="text-break">{props.info[item]}</td>
                </tr>
              ))}
          </tbody>
        </table>
      </div>
    </div>
  </div>
);

export default Section;
