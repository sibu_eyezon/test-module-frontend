import axios from 'axios';
import { API } from '../../config/config';
import store from '../../store';
const MAX_QUESTION_LIMIT = 1;
class Operations {
  constructor(test_id) {
    this._testID = test_id;
    this._question = {};
    this._options = [];
    this._totalQuestion = 0;
    this._perPageQuestion = MAX_QUESTION_LIMIT;
    this._currentPage = 1;
  }

  async _getSections(token = null) {
    try {
      let id = this._testID;
      let { data } = await axios.post(
        `${API}/exam_api/section/`,
        { id },
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      );
      this._defaultSection = data[0].id;
      const payload = {
        defaultSection: this._defaultSection,
        sections: data,
      };
      store.dispatch({
        type: 'SET_SECTION',
        payload: payload,
      });
      return payload;
    } catch (e) {}
  }

  _changeCurrentPage = (page, id, token) => {
    this._currentPage = page;
    return this._getQuestion(id, token);
  };

  _next = (total, id, token) => {
    if (this._currentPage < total) {
      this._currentPage++;
      return this._getQuestion(id, token);
    }

    return this._getQuestion(id, token);
  };

  _prev = (id, token) => {
    if (this._currentPage > 1) {
      this._currentPage--;
      return this._getQuestion(id, token);
    }
    return this._getQuestion(id, token);
  };

  _changeSection = (id, token) => {
    this._currentPage = 1;
    return this._getQuestion(id, token);
  };

  async _getQuestion(id, token = null) {
    try {
      let defaultData = {
        id: id,
        page: this._currentPage,
        maxLimit: this._perPageQuestion,
      };

      let { data } = await axios.post(
        `${API}/exam_api/question/`,
        defaultData,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      );
      this._totalQuestion = data.totalRecords;
      this._question = data.question;
      this._options = data.options;
      const payload = {
        records: this._totalQuestion,
        question: this._question,
        options: this._options,
        currentPage: this._currentPage,
        next: this._next,
        prev: this._prev,
        pageChange: this._changeCurrentPage,
        sectionChange: this._changeSection,
      };
      store.dispatch({
        type: 'SET_QUESTION_DATA',
        payload: payload,
      });

      return payload;
    } catch (e) {}
  }
}

export default Operations;
