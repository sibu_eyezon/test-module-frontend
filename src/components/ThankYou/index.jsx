import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import classes from './ThankYou.module.css';
import { useDispatch } from 'react-redux';
function Thankyou() {
  const auth = useSelector((state) => state.auth);
  const test = useSelector((state) => state.test);
  const dispatch = useDispatch();
  let history = useHistory();
  const logout = async () => {
    dispatch({ type: 'LOGOUT' });
    history.push('/');
  };

  return (
    <div className="container">
      <div className={classes.Container_Box}>
        <div className="row">
          <div className="col-md-5 m-auto">
            <div className="card">
              <div className="card-header p-3">
                <div className="card-title text-center">
                  <h3>Thank You</h3>
                </div>
              </div>
              <div className="card-body">
                <p className="card-text text-justify">
                  <strong>{auth?.userDetails?.user_info?.Name}</strong>, you
                  have successfully completed this{' '}
                  <strong>{test.test?.Title}</strong> examination. Please click
                  the below button to make sure to finish this exam
                </p>
              </div>
              <div className="card-footer text-center">
                <button className="btn btn-primary" onClick={logout}>
                  Finished Exam
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Thankyou;
