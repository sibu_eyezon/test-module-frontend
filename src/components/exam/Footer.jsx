import React from 'react';

const Footer = ({ goPrev, goNext }) => {
  return (
    <div className="footer footer-end">
      <div className="row " id="down">
        <div className="col-md-4 col-6  text-left ">
          <button type="button" className="btn btn-info" onClick={goPrev}>
            Back
          </button>
        </div>

        <div className="col-md-4 col-6  text-right ">
          <button type="button" className="btn btn-info" onClick={goNext}>
            Next
          </button>
        </div>
      </div>
    </div>
  );
};

export default Footer;
