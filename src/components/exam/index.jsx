import React, { Component, Fragment } from 'react';
import { Redirect } from 'react-router-dom';
import './Exam.css';
import swal from 'sweetalert';
import Operations from '../js';
import Nav from './Nav';
import Footer from './Footer';
import Question from './Question';
import Option from './Option';
import Button from './Button';

import { connect } from 'react-redux';
import { endTest } from '../../actions/test';

class TestBox extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sectionData: {},
      questionData: {},
    };
  }

  operations =
    this.props.auth.user && new Operations(this.props.auth.user?.test_id);

  changePage = (page) => {
    this.setState(
      {
        questionData: { ...this.state.questionData, currentPage: page },
      },
      () => {
        this.state.questionData
          ?.pageChange(
            page,
            this.state.sectionData?.defaultSection,
            this.props.auth.user?.token
          )
          .then((data) => {
            this.setState({
              questionData: data,
            });
          });
      }
    );
  };

  getChangeSectionByNP = (id) => {
    this.setState(
      {
        sectionData: { ...this.state.sectionData, defaultSection: id },
      },
      () => {
        this.state.questionData
          .sectionChange(
            this.state.sectionData.defaultSection,
            this.props.auth.user?.token
          )
          .then((data) => {
            this.setState({ questionData: data });
          });
      }
    );
  };

  getChangeSectionByNext = (id) => {
    let sid = parseInt(id);
    let section = this.state.sectionData.sections;
    for (let i = 0; i < section.length; i++) {
      if (section[i].id === sid) {
        if (section[i + 1]) {
          swal({
            title: `${this.props.auth?.userDetails?.user_info?.Name}`,
            text: 'Are you sure, you want to go to next section?',
            icon: 'warning',
            buttons: true,
            successMode: true,
          }).then((next) => {
            if (next) {
              this.getChangeSectionByNP(section[i + 1].id);
            }
          });
        } else {
          swal({
            title: `${this.props.auth?.userDetails?.user_info?.Name}`,
            text: 'Are you sure, you want to end this exam?',
            icon: 'warning',
            buttons: true,
            successMode: true,
          }).then((next) => {
            if (next) {
              this.props.endTest();
            }
          });
        }
      }
    }
  };

  getChangeSectionByPrevious = (id) => {
    let sid = parseInt(id);
    let section = this.state.sectionData.sections;

    for (let i = 0; i < section.length; i++) {
      if (section[i].id === sid) {
        if (section[i - 1]) {
          swal({
            title: `${this.props.auth?.userDetails?.user_info?.Name}`,
            text: 'Are you sure, you want to go back to previous section?',
            icon: 'warning',
            buttons: true,
            successMode: true,
          }).then((prev) => {
            if (prev) {
              this.getChangeSectionByNP(section[i - 1].id);
            }
          });
        } else {
          swal({
            title: `${this.props.auth?.userDetails?.user_info?.Name}`,
            text: "This is the first question of this exam. You can't go back.",
            icon: 'error',
            dangerMode: true,
          });
        }
      }
    }
  };

  goNext = () => {
    this.setState(
      {
        questionData: {
          ...this.state.questionData,
          currentPage: this.state.questionData.currentPage + 1,
        },
      },
      () => {
        if (
          this.state.questionData.records < this.state.questionData.currentPage
        ) {
          this.getChangeSectionByNext(this.state.sectionData.defaultSection);
        }

        this.state.questionData
          .next(
            this.state.questionData.records,
            this.state.sectionData.defaultSection,
            this.props.auth.user?.token
          )
          .then((data) => {
            this.setState({
              questionData: data,
            });
          });
      }
    );
  };

  goPrev = () => {
    this.setState(
      {
        questionData: {
          ...this.state.questionData,
          currentPage: this.state.questionData.currentPage - 1,
        },
      },
      () => {
        if (this.state.questionData.currentPage === 0) {
          this.getChangeSectionByPrevious(
            this.state.sectionData.defaultSection
          );
        }
        this.state.questionData
          .prev(
            this.state.sectionData.defaultSection,
            this.props.auth.user?.token
          )
          .then((data) => {
            this.setState({
              questionData: data,
            });
          });
      }
    );
  };

  changeSection = (e) => {
    this.setState(
      {
        sectionData: {
          ...this.state.sectionData,
          defaultSection: e.target.value,
        },
      },
      () => {
        this.state?.questionData
          .sectionChange(
            this.state.sectionData.defaultSection,
            this.props.auth.user?.token
          )
          .then((data) => {
            this.setState({ questionData: data });
          });
      }
    );
  };

  componentDidMount() {
    if (this.operations) {
      this.operations._getSections(this.props.auth.user?.token).then((data) => {
        this.setState(
          {
            sectionData: data,
          },
          () => {
            this.operations
              ._getQuestion(
                this.state.sectionData?.defaultSection,
                this.props.auth.user?.token
              )
              .then((data) => {
                this.setState({ questionData: data });
              });
          }
        );
      });
    }
  }

  render() {
    if (!this.props.auth.user && !this.props.test.test) {
      return <Redirect to="/" />;
    } else if (this.props.auth.user && !this.props.test.test) {
      return <Redirect to="/details" />;
    }

    return (
      <div className="exam-container">
        <div className="content">
          <Nav
            defaultSection={this.props.test?.section?.defaultSection}
            sections={this.props.test?.section?.sections}
            changeDefaultSection={this.changeSection}
            totalRecords={this.props.test?.question_data?.records}
            quesNo={this.props.test?.question_data?.currentPage}
            endTheTest={this.props.endTest}
          />
          <div className="container-fluid ">
            <div className="row content">
              <Question
                quesNo={this.props.test?.question_data?.currentPage}
                question={this.props.test?.question_data?.question}
              />
              <Option
                section={this.props.test?.section?.defaultSection}
                options={this.props.test?.question_data?.options}
                question={this.props.test?.question_data?.question}
              />
              <Button
                totalRecords={this.props.test?.question_data?.records}
                currentPage={this.props.test?.question_data?.currentPage}
                changePage={this.changePage}
              />
            </div>
          </div>
          <div className="row" id="instruction">
            <div className="col-md-10 col-lg-8 text-center ">
              <h5>Test Instructions</h5>
              1.All Question are compulsory
              <br />
              2.All the best
              <br />
            </div>
          </div>
        </div>
        <Footer goPrev={this.goPrev} goNext={this.goNext} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const auth = state.auth;
  const alert = state.alert;
  const test = state.test;
  return {
    auth,
    alert,
    test,
  };
};
const mapDispatchToProps = {
  endTest: (data) => endTest(data),
};
export default connect(mapStateToProps, mapDispatchToProps)(TestBox);
