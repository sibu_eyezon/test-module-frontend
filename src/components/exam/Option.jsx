import React, { Component } from 'react';
import axios from 'axios';
import { API } from '../../config/config';
import { connect } from 'react-redux';

class Option extends Component {
  constructor(props) {
    super(props);
    let { test_can, pub_id } = this.props.auth.user;
    this.state = {
      givenAns: 0,
      test_can,
      pub_id,
    };
  }
  onChangeHandler = async (e) => {
    let reqData = {
      test_can: this.state.test_can,
      pub_id: this.state.pub_id,
      sec_id: this.props.section,
      ques_id: this.props.question.id,
      option_id: e.target.value,
      ques_marks: this.props.question.marks,
    };
    axios
      .post(`${API}/exam_api/give_answer/`, reqData, {
        headers: { Authorization: `Bearer ${this.props.auth.user?.token}` },
      })
      .catch((e) => {});
    this.setState({ givenAns: e.target.value });
  };

  _getGivenAns = async () => {
    if (this.props.question && this.props.section) {
      let reqData = {
        id: this.props.question.id,
        section: this.props.section,
        test_can: this.state.test_can,
        pub_id: this.state.pub_id,
      };
      axios
        .post(`${API}/exam_api/question_attemp`, reqData, {
          headers: { Authorization: `Bearer ${this.props.auth.user?.token}` },
        })
        .then((res) => {
          if (res.status === 200) {
            this.setState({
              givenAns: res?.data?.option_id,
            });
          }
        })
        .catch((e) => {});
    }
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.section !== undefined && prevProps.question !== undefined) {
      if (prevProps.question.id !== this.props.question.id) {
        this._getGivenAns();
      }
    }
  }

  componentDidMount() {
    this._getGivenAns();
  }

  render() {
    let optionsList = () =>
      this.props.options &&
      this.props.options.map((item, i) => {
        return (
          <li key={i}>
            <input
              className="mr-3"
              type="radio"
              value={item.id}
              onChange={this.onChangeHandler}
              checked={parseInt(this.state.givenAns) === item.id}
            />
            {`${item.body.replace(/(<([^>]+)>)|&nbsp;/gi, '')}`}
          </li>
        );
      });
    return (
      <div className="col-md-4 col-sm-6 text-left border-box">
        <h5>Options are as follows</h5>
        <ol className="form-check">{optionsList()}</ol>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  const auth = state.auth;
  const test = state.test;
  return {
    auth,
    test,
  };
};
export default connect(mapStateToProps)(Option);
