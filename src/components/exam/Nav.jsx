import moment from 'moment';
import React, { useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';

const Nav = ({
  defaultSection,
  sections,
  changeDefaultSection,
  quesNo,
  totalRecords,
  endTheTest,
}) => {
  const test = useSelector((state) => state.test);
  const auth = useSelector((state) => state.auth);
  const loading = useSelector((state) => state.loading);
  const timerInterval = useRef();

  const [hour, sethour] = useState(0);
  const [minute, setminute] = useState(0);
  const [second, setsecond] = useState(0);
  const endTime = moment.utc(test.test_info.test_end_dttm).local();
  let sTime = moment.utc(test.test_info.test_start_dttm).local();
  let eTime = moment.utc(test.test_info.test_end_dttm).local();

  const timer = () => {
    const currentTime = moment();
    let diffTime = endTime.diff(currentTime);
    diffTime = moment.duration(diffTime, 'milliseconds');
    sethour(diffTime.hours());
    setminute(diffTime.minutes());
    setsecond(diffTime.seconds());
    if (diffTime.valueOf() < 1000) {
      clearInterval(timerInterval.current);
      endTheTest();
    }
  };
  useEffect(() => {
    timerInterval.current = setInterval(timer, 1000);
    return () => {
      clearInterval(timerInterval.current);
    };
  }, []);

  let optionList = () =>
    defaultSection &&
    sections &&
    sections.map((section, i) => {
      return (
        <option key={i} value={section.id}>
          {section.section_name}
        </option>
      );
    });

  return (
    <nav>
      <div className="container-fluid ">
        <div className="row text-center header align-items-center">
          <div className="col-sm-3">
            <h3 className="text-white">
              <strong>{test.test.Title}</strong>
            </h3>
          </div>
          <div className="col-sm-3">
            <h5>
              <strong>{auth?.userDetails?.user_info?.Name}</strong>
            </h5>
          </div>

          <div className="col-sm-3">
            <h5>
              <strong>
                Time Left {hour}:{minute}:{second}{' '}
              </strong>
            </h5>
          </div>
          <div className="col-sm-3">
            <button
              type="button"
              className="btn btn-warning"
              onClick={endTheTest}
              disabled={loading.isLoading}
            >
              {loading.isLoading ? 'Loading ...' : 'Click to end test'}
            </button>
          </div>
        </div>
        <div className="row timing">
          <div className="col-md-4 text-center">
            <div className="row ">
              <div className="col-sm-6 form-group">
                <select
                  className="form-control"
                  id="select-button"
                  onChange={changeDefaultSection}
                >
                  {optionList()}
                </select>
              </div>
              <div className="col-sm-6">
                <span>
                  Question {quesNo}/{totalRecords}
                </span>
              </div>
            </div>
          </div>
          <div className="col-md-8 text-danger">
            <strong>
              <div className="row text-center">
                <div className="col-sm-4">
                  Start Time : {sTime.format('h:mm:ss a')}{' '}
                </div>
                <div className="col-sm-4">
                  End Time :{eTime.format('h:mm:ss a')}
                </div>
                <div className="col-sm-4">
                  Test Duaration:
                  {test.test.Time} Min
                </div>
              </div>
            </strong>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Nav;
