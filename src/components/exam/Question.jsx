import React from 'react';

function Question({ quesNo, question }) {
  return (
    <div className="col-md-5 col-sm-6 sidenav border-box">
      <p>
        <span>
          {`${quesNo && quesNo}. ${
            question && question.qbody.replace(/(<([^>]+)>)|&nbsp;/gi, '')
          }`}
        </span>
      </p>
      <div className="instruction">
        <p>Section Instructions</p>
      </div>
    </div>
  );
}

export default Question;
