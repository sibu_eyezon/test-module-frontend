const initialState = {
  isLoading: false,
};
const loadingReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_LOADING':
      return {
        isLoading: true,
      };

    case 'UNSET_LOADING': {
      return {
        isLoading: false,
      };
    }

    default:
      return state;
  }
};
export default loadingReducer;
