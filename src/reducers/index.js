import storage from 'redux-persist/lib/storage';
import { combineReducers } from 'redux';
import auth from './authReducer';
import test from './testReducer';
import alert from './alertReducer';
import loading from './loadingReducer';
const appReducer = combineReducers({
  auth,
  test,
  alert,
  loading,
});

const rootReducer = (state, action) => {
  if (action.type === 'LOGOUT') {
    storage.removeItem('persist:root');
    return appReducer(undefined, action);
  }
  return appReducer(state, action);
};
export default rootReducer;
