
const initialState={
    test:null,
    test_info:null,
    question_data:null,
    section:null
}
const testReducer = (state = initialState, action) => {
 
  switch (action.type) {
    case 'SET_TEST':
      return {
        ...state,
        test: action.payload
      };

 
    case 'SET_TEST_INFO': {
      return {
        ...state,
        test_info: action.payload
      };
      
    }
    case 'SET_QUESTION_DATA':{
      return {
        ...state,
        question_data:action.payload
      }
    }
     case 'SET_SECTION':{
      return {
        ...state,
        section:action.payload
      }
    }
    case 'REMOVE_ALL':
      return {
        
      };
    default:
      return state;
  }
 
};
export default testReducer;