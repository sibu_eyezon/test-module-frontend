const initialState = {
  user: null,
  userDetails: null,
  isAuthenticated: false,
};
const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN':
      return {
        ...state,
        user: action.payload,
        isAuthenticated: true,
      };

    case 'LOGOUT': {
      return {
        ...state,
        user: null,
        isAuthenticated: false,
      };
    }
    case 'SET_USER':
      return {
        ...state,
        userDetails: action.payload,
      };
    case 'ERROR':
      return {
        ...state,
        Error: action.payload,
        isAuthenticated: false,
      };
    default:
      return state;
  }
};
export default authReducer;
