
const initialState={
    message:null,
    isAlert:false
}
const alertReducer = (state = initialState, action) => {
 
  switch (action.type) {
    case 'SET_ALERT':
      return {
        ...state,
        isAlert: true,message:action.payload.errorMessage
      };

 
    case 'REMOVE_ALERT': {
      return {
        isAlert:false
      };
      
    }
   
    default:
      return state;
  }
 
};
export default alertReducer;