import axios from 'axios';
import { API } from '../config/config';
import moment from 'moment';
import { toast } from 'react-toastify';

export const startTest = ({ id, duration, token = null }) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: 'SET_LOADING',
      });
      let time = moment();
      axios
        .post(
          `${API}/details_api/start_exam/`,
          { id, time, duration },
          {
            headers: { Authorization: `Bearer ${token}` },
          }
        )
        .then((res) => {
          dispatch({
            type: 'UNSET_LOADING',
          });
          let data = res.data;

          dispatch({
            type: 'SET_TEST_INFO',
            payload: data,
          });
        })
        .catch((err) => {
          dispatch({
            type: 'UNSET_LOADING',
          });
          if (err.response?.data?.message) {
            toast.error(err.response?.data?.message);
          } else if (err?.response?.status === 404) {
            toast.error(err.response?.data?.message || 'Test Not found');
          }
        });
    } catch (error) {
      dispatch({
        type: 'UNSET_LOADING',
      });
      toast.error('Something went wrong');
    }
  };
};

export const endTest = () => {
  return async (dispatch, getStore) => {
    try {
      dispatch({
        type: 'SET_LOADING',
      });
      const state = getStore();
      let id = state.auth.user.test_can;
      let token = state.auth.user.token;
      axios
        .post(
          `${API}/exam_api/finished/`,
          { id },
          {
            headers: { Authorization: `Bearer ${token}` },
          }
        )
        .then((res) => {
          dispatch({
            type: 'UNSET_LOADING',
          });
          let data = res.data;
          if (data) {
            window.location.href = '/thankyou';
          }
        })
        .catch((err) => {
          dispatch({
            type: 'UNSET_LOADING',
          });
          if (err?.response?.status === 404) {
            dispatch({
              type: 'SET_ALERT',
              payload: { errorMessage: 'Test Not Found' },
            });
          }
        });
    } catch (error) {
      dispatch({
        type: 'UNSET_LOADING',
      });
      dispatch({
        type: 'ERROR',
        payload: error,
      });
    }
  };
};
