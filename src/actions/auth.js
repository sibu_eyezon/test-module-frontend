import axios from 'axios';
import { toast } from 'react-toastify';
import { API } from '../config/config';

export const login = ({ passcode }) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: 'SET_LOADING',
      });

      axios
        .post(`${API}/auth_api/`, { passcode })
        .then((res) => {
          let data = res.data;

          dispatch({
            type: 'LOGIN',
            payload: data,
          });
          dispatch({
            type: 'UNSET_LOADING',
          });
        })
        .catch((err) => {
          dispatch({
            type: 'UNSET_LOADING',
          });
          if (err.response?.data?.message) {
            toast.error(err.response?.data?.message);
          } else {
            if (err?.response?.status === 403) {
              toast.error('This passcode is already used');
            }
          }
        });
    } catch (error) {
      dispatch({
        type: 'UNSET_LOADING',
      });
      dispatch({
        type: 'ERROR',
        payload: error,
      });
    }
  };
};

export const logout = () => {
  return async (dispatch) => {
    dispatch({ type: 'LOGOUT' });
  };
};

export const setAuth = (data) => {
  return async (dispatch) => {
    dispatch({
      type: 'LOG',
      payload: data,
    });
  };
};

export const cancelAlert = () => {
  return async (dispatch) => {
    dispatch({ type: 'REMOVE_ALERT' });
  };
};
