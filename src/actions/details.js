import axios from 'axios';
import { API } from '../config/config';

export const getUserDetails = ({ id, token = null }) => {
  return async (dispatch) => {
    try {
      axios
        .post(
          `${API}/details_api/user_details/`,
          { id },
          {
            headers: { Authorization: `Bearer ${token}` },
          }
        )
        .then((res) => {
          let data = res.data;

          dispatch({
            type: 'SET_USER',
            payload: data,
          });
        })
        .catch((err) => {
          if (err?.response?.status === 404) {
            dispatch({
              type: 'SET_ALERT',
              payload: { errorMessage: 'User Not Found' },
            });
          }
        });
    } catch (error) {
      dispatch({
        type: 'ERROR',
        payload: error,
      });
    }
  };
};

export const getTestDetails = ({ id, token = null }) => {
  return async (dispatch) => {
    try {
      axios
        .post(
          `${API}/details_api/exam_details/`,
          { id },
          {
            headers: { Authorization: `Bearer ${token}` },
          }
        )
        .then((res) => {
          let data = res.data;

          dispatch({
            type: 'SET_TEST',
            payload: data,
          });
        })
        .catch((err) => {
          if (err?.response?.status === 404) {
            dispatch({
              type: 'SET_ALERT',
              payload: { errorMessage: 'User Not Found' },
            });
          }
        });
    } catch (error) {
      dispatch({
        type: 'ERROR',
        payload: error,
      });
    }
  };
};
